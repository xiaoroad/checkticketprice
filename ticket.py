# -*- coding: UTF-8 -*-
# 票通1029: http://www.ticket.co.jp/sys/d/200.htm?st=20161029&orderable=1&order=price
# 票通1030: http://www.ticket.co.jp/sys/d/200.htm?st=20161030&orderable=1&order=price
# 票通1013: http://www.ticket.co.jp/sys/d/200.htm?st=20161013&orderable=1&order=price
# 票通1014: http://www.ticket.co.jp/sys/d/200.htm?st=20161014&orderable=1&order=price
__author__ = 'zhangxiaolu'
import urllib2
import chardet
import re
import time
import smtplib
from email.mime.text import MIMEText

url = urllib2.Request((raw_input("输入url: ")))
expected = int(raw_input("预期价格: "))
second = int(raw_input("刷新间隔时间: "))
mail_sub = str(raw_input("邮件主题: "))

hasAlertedList = [] # 已经警报过的列表
index = 0

#要发给谁
mailto_list = ["[邮箱地址]"]

#设置服务器，用户名、口令以及邮箱的后缀 qq配置里有
mail_host = "smtp.qq.com"  #需开启smtp服务
mail_user = "[email用户名]" #email用户名
mail_pass = "[email smtp口令]"
mail_postfix = "qq.com"

def task():
    global index
    index += 1
    sock = urllib2.urlopen(url).readlines()
    idList = [] # 所有id列表
    priceList = [] # 所有价格列表, 索引与id列表匹配
    filterPriceList = [] # 符合预期价格的列表
    alertList = [] # 需要警报的列表
    for line in sock:
        line = line.strip().strip('\n')
        # print(chardet.detect(line)["encoding"])
        if chardet.detect(line)["encoding"]:
            line = line.decode(chardet.detect(line)["encoding"]).encode('UTF-8')
            if re.search(r'js-ticket_id', line):
                id = re.findall(ur"\"\d+\"", line)
                if id:
                    idList.append(int(re.sub(r'\D', "", id[0])))
            line1 = re.search(r'"ticket-price"', line)
            if line1:
                price = re.findall(ur"\d+,\d+</", line)
                if price:
                    priceList.append(int(re.sub(r'\D', "", price[0])))
    for i, val in enumerate(priceList):
        if val <= expected:
            filterPriceList.append(val)
            # 防止重复提醒
            if hasAlertedList.count(idList[i]) == 0:
                alertList.append(idList[i])

    if len(alertList) != 0:
        # 发邮件提醒
        # print(alertList)
        # print(filterPriceList)
        # idStr = ','.join(str(x) for x in alertList)
        priceStr = ','.join(str(x) for x in filterPriceList)
        str1 = 'price: ' + priceStr
        print(str1)
        if __name__ == '__main__':
            if send_mail(mailto_list, mail_sub, str1):
                print "发送成功"
            else:
                print "发送失败"

        hasAlertedList.extend(alertList)
    else:
        print('无新提醒' + str(index))

def send_mail(to_list, sub, content):
#'''''
#to_list:发给谁
#sub:主题
#content:内容
#'''''
    me = mail_user+"@"+mail_postfix
    msg = MIMEText(content)
    msg['Subject'] = sub #设置主题
    msg['From'] = me #发件人
    msg['To'] = ";".join(to_list) #收件人
    try:
        s = smtplib.SMTP_SSL("smtp.qq.com", 465)
        s.connect(mail_host)
        s.login(me, mail_pass)
        s.sendmail(me, to_list, msg.as_string())
        s.close()
        return True
    except Exception, e:
        print str(e)
        return False

def timer(n = 1):
    while True:
        task()
        time.sleep(n)
timer(second)
# task()